var root = document.location.href;

var join_path = function(left, right) {
	return left.replace(/\/+$/, '') + '/' + right.replace(/^\/+/, '');
};

var save_state = function(directory)
{
	// Save which folders were open
	if (history.pushState) {
		var path = $.map(
			$(directory).parents('.directory').children('a'),
			function(el) { return $(el).text(); })
			.reverse();

		// If we just closed the folder, link to the parent folder
		if (!$(directory).next('ul').is(':visible'))
			path.pop();

		var label = $(path).last().get(0);

		var state = {
			open: $('.directory > ul')
				.filter(':visible')
				.closest('.directory')
				.map(function() {
					return this.id;
				})
				.toArray()
		};

		history.pushState(state, label,
			join_path(root, path.join('/') + '/'));
	}
};

// Restore which folders are open
var restore_state = function(state) {
	// First, reset everything to its default state
	$('.directory > ul').hide();
	$('.directory').removeClass('open');
	$('.file, .directory').show();
	$('.search input[name=q]').val('');

	// Then, open the selected folders again
	if (state && state.open)
		$.map(state.open, function(id) {
			$('#' + id)
				.children('ul').show().end()
				.parents('.directory > ul').show()
				.parents('.directory').addClass('open');
		});

	// Or search the query again
	if (state && state.query) {
		$('.search input[name=q]').val(state.query);
		search(state.query);
	}
};

// Click a directory to expand it.
jQuery(function($) {
	//$preview = $('<div class="preview">').appendTo(document.body);

	$('.directory > ul').hide();

	$('.directory, .file').keydown(function(e) {
		switch (e.keyCode)
		{
			case 13: // Enter
				e.preventDefault();
				e.stopPropagation();
				if ($(this).hasClass('file'))
					window.open($(this).children('a').attr('href'));
				else
					$(this).children('a').click();
				break;

			/*
			case 32: // Space
				if ($(this).data('preview')) {
					e.preventDefault();
					e.stopPropagation();
					$preview.load($(this).data('preview'));
				}
				break;
			*/

			case 38: // Up
				e.preventDefault();
				e.stopPropagation();

				if ($(this).prev('li:visible').length)
					$(this).prev('li:visible').focus();
				else
					$(this).closest('ul:visible').closest('li:visible').first().focus();
				break;

			case 37: // Left
				e.preventDefault();
				e.stopPropagation();
				$(this).closest('ul:visible').closest('li:visible').first().focus();
				break;

			case 40: // Down
				e.preventDefault();
				e.stopPropagation();

				// If there is a child folder opened, move into it
				if ($(this).children('ul:visible').length)
					$(this).children('ul:visible').children('li:visible').first().focus();
				// If this is the end of the list, move to the parent (or their parent) next sibling
				else if ($(this).next('li:visible').length == 0) {
					var $el = $(this);
					while ($el.next('li:visible').length == 0 && $el.closest('ul:visible').length)
						$el = $el.closest('ul:visible').closest('li:visible');
					$el.next('li:visible').focus();
				}// Otherwise, just try to move to the next
				else
					$(this).next('li:visible').focus();
				break;
		}
	});
	
	$('.directory > a').click(function(e) {
		if (e.shiftKey || e.ctrlKey || e.metaKey || e.altKey)
			return;
		
		e.preventDefault();

		$(this).next('ul').toggle();
		$(this).parent().toggleClass('open');

		save_state(this);
	});
});

// When using history, check if there are folders to re-open
window.onpopstate = function(e) {
	restore_state(e.state);
};

// Hide the search fields till the search button is clicked
$('.search').addClass('collapsed');

$('.search > input').focus(function(e) {
	e.preventDefault()
	$(this).closest('.search').removeClass('collapsed');
	$('.search > input').get(0).focus();
});

$('.search > input').focusout(function(e) {
	e.preventDefault()
	$(this).closest('.search').addClass('collapsed');
});

$('a#toggle-search').click(function(e) {
	e.preventDefault();
	$(this).closest('.search').removeClass('collapsed');
	$('.search > input').get(0).focus();
});

// Search (or better name: filter)
var search = function(query) {
	$.getJSON(window.search_url, {'q': query, 'output': 'json'}, function(response) {
		// Hide all results for now
		$('.directory > ul, .file, .directory').hide();
		$('.directory').removeClass('open');
		
		$('.file').each(function() {
			var is_hit = typeof response[this.id] !== 'undefined';

			// Highlight the file if it is a hit
			$(this).toggle(is_hit);

			if (is_hit)
			{
				// If there is an excerpt, add it
				if (typeof response[this.id] === 'string' && response[this.id].length > 0)
					$('<span>')
						.addClass('search-excerpt')
						.html(response[this.id])
						.appendTo(this);

				// Make sure results are visible
				$(this).parents('.directory, .directory > ul').show();
				$(this).parents('.directory').addClass('open');
			}
		});
	});
};

var stop_search = function() {
	$('.directory > ul').hide();
	$('.directory').removeClass('open');
	$('.file, .directory').show();
	$('.file > .search-excerpt').remove();
};

$('.search')
	.submit(function(e) {
		e.preventDefault();

		var val = $(this).find('input[name=q]').val();

		// Clear the search state
		stop_search();

		// If there is a query, search!
		if (val != '') {
			if (history.pushState)
				history.pushState(
					{query: val},
					'Search for ' + val,
					join_path(root, window.search_url+'?q=' + val));

			search(val);
		}
	})
	.keyup(function(e) {
		// if escape key is pressed, clear and reset
		if (e.keyCode == 27) {
			e.preventDefault();
			$(this).val('');
			stop_search();

			// If we have pushState enabled, reset to root.
			if (history.pushState)
				history.pushState({open: []}, '', root);
		}
	});
