CREATE TABLE `files` (
  `uid` char(32) NOT NULL,
  `path` text NOT NULL,
  `keywords` text NOT NULL,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

