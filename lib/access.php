<?php

/**
 * Parse a selector configuration file
 *
 * @param string $path to the file
 * @return array map of selectors
 */
function read_selectors($path)
{
	$selectors = array();

	foreach (file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) as $line)
	{
		$line = trim($line);

		// Ignore comments
		if ($line[0] == '#')
			continue;

		list($pattern, $group) = preg_split('/(?<!\\\\)\s+/', $line, 2);

		// Remove escaping characters and add the pattern to the list
		$selectors[str_replace('\\', '', $pattern)] = array_map('trim', explode(',', $group));
	}

	return $selectors;
}

/**
 * Deduce a group name given a path and a bunch of selectors.
 *
 * Examples:
 *     /Minutes => 'None'
 *     /Minutes/**\/*.pdf => 'Bestuur'
 *     Now '/Minutes/test.txt' will return 'None' and both '/Minutes/test/x.pdf'
 *     and '/Minutes/test/test2/x.pdf' will return 'Bestuur'.
 * 
 * @param string $path the path
 * @param array $selectors a map of selectors in which the key is the pattern and the value the group name
 * @return string groupname of found, NULL if nothing matched
 */
function path_get_access_group($path, array $selectors)
{
	$parts = explode(DIRECTORY_SEPARATOR, trim($path, DIRECTORY_SEPARATOR));

	$groups = null;

	while ($part = array_shift($parts))
	{
		// Selectors for the next round
		$new_selectors = array();

		foreach ($selectors as $selector => $selector_group)
		{
			$selector_parts = explode('/', trim($selector, '/'), 2);

			// Empty (e.g. '/')
			if ($selector_parts[0] == '')
				$groups = $selector_group;

			// Directory wildcard match
			else if ($selector_parts[0] == '**')
			{
				// Keep the original pattern because the ** stays valid in directories
				$new_selectors[$selector] = $selector_group;
				// But also remove the ** part as we may apply this pattern in the next directory
				$new_selectors[$selector_parts[1]] = $selector_group;
			}

			// One-on-one match
			else if (fnmatch($selector_parts[0], $part))
			{
				// If this is the end of a pattern, apply the group
				if (!isset($selector_parts[1]))
					$groups = $selector_group;
				// Otherwise use the rest of the pattern for the rest of the filename
				else
					$new_selectors[$selector_parts[1]] = $selector_group;
			}
		}

		// If there are no new selectors, the group won't change any more
		if (!count($new_selectors))
			break;

		$selectors = $new_selectors;
	}

	return $groups;
}
