<?php

function human_readable_filesize($size)
{
	$mod = 1024;

	$units = explode(' ','B kB MB GB TB');
	
	for ($i = 0; $size > $mod; $i++) {
		$size /= $mod;
	}

	return round($size, 2) .' '. $units[$i];
}
