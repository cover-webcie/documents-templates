#!/usr/bin/php
<?php

require_once dirname(__FILE__) . '/config.php';

$preprocessors = array(
	'*.(txt|tex)' => function($path) {
		return file_get_contents($path);
	},
	'*.html' => function($path) {
		return strip_tags(file_get_contents($path));
	},
	'*.(pdf|doc|docx|ppt|pptx|odf|odt|zip|xls|xlsx)' => function($path) {
		return shell_exec('java -jar ' . escapeshellarg(dirname(__FILE__) . '/bin/tika-app-2.8.0.jar') . ' --text ' . escapeshellarg($path));
	}
);

function wildcard_match($pattern, $subject)
{
	$pattern = strtr($pattern, array(
		'*' => '.*?', // 0 or more (lazy) - asterisk (*)
		'?' => '.', // 1 character - question mark (?)
		'.' => '\.'
	));

	return preg_match("/$pattern/", $subject);
}

function preprocess($path)
{
	global $preprocessors;

	$keywords = array();

	foreach ($preprocessors as $pattern => $preprocessor)
		if (wildcard_match($pattern, $path))
			$keywords[] = call_user_func($preprocessor, $path);

	return implode(' ', $keywords);
}

function index_get_records()
{
	$db = get_db();
	
	$stmt = $db->query("SELECT uid, path, keywords, UNIX_TIMESTAMP(last_modified) as last_modified FROM files");
	$stmt->setFetchMode(PDO::FETCH_OBJ);

	$records = array();

	foreach ($stmt as $record)
		$records[$record->uid] = $record;

	return $records;
}

function index_insert_record($path, $keywords)
{
	$db = get_db();
	
	$db->prepare("INSERT INTO files (uid, path, keywords, last_modified) VALUES(:uid, :path, :keywords, NOW())")->execute(array(':uid' => md5($path), ':keywords' => $keywords, ':path' => $path));
}

function index_update_record($path, $keywords)
{
	$db = get_db();
	
	$db->prepare("UPDATE files SET keywords = :keywords, last_modified = NOW() WHERE uid = :uid")->execute(array(':keywords' => $keywords, ':uid' => md5($path)));
}

function index_delete_records(array $paths)
{
	$db = get_db();

	$db->query("DELETE FROM files WHERE uid IN (" . implode(',', array_map(array($db, 'quote'), $paths)) . ")");
}

function index_documents($root, $force = false)
{
	$iterator = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($root, FilesystemIterator::FOLLOW_SYMLINKS));

	$records = index_get_records();

	foreach ($iterator as $file)
	{
		// Skip everything that is not a file.
		if (!$file->isFile())
			continue;

		$uid = md5($file->getPathname());

		// File is not yet indexed, add it as a new record
		if (!isset($records[$uid]))
			index_insert_record($file->getPathname(), preprocess($file->getPathname()));
		
		// Record already exists
		else
		{
			$record = $records[$uid];

			// Remove it form the list. Eventually, the list will only
			// contain records that point to files that do not exist anymore.
			unset($records[$uid]);

			// If the record is up to date, skip it.
			if ($record->last_modified < $file->getMTime() || $force)
				index_update_record($file->getPathname(), preprocess($file->getPathname()));
		}
	}

	index_delete_records(array_keys($records));
}

index_documents(SD_ROOT, $argc > 1 && $argv[1] == '-f');
