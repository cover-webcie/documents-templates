<?php

require 'config.php';

error_reporting(E_ALL);
ini_set('display_errors', true);

date_default_timezone_set('Europe/Amsterdam');

require_once 'lib/functions.php';
require_once 'lib/session.php';
require_once 'lib/access.php';
require_once 'lib/search.php';
require_once 'lib/human.php';

if (!$file_permissions = read_selectors('public.txt'))
	die('Cannot read or parse permissions file');

function request_from_cover_gateway()
{
	return $_SERVER['REMOTE_ADDR'] == '129.125.5.57'; // SLACK Gateway
}

function logged_in()
{
	return request_from_cover_gateway() || cover_session_logged_in();
}

function fsencode_path($path, $root = SD_ROOT)
{
	$parts = array_filter(explode('/', $path));

	array_unshift($parts, $root);

	return implode(DIRECTORY_SEPARATOR, $parts);
}

function urlencode_path($path)
{
	$path = preg_replace('{^' . preg_quote( SD_ROOT ) . '}', '', $path);

	$parts = explode(DIRECTORY_SEPARATOR, $path);

	$parts = array_map('rawurlencode', $parts);

	return implode('/', $parts);
}

function encode_name($name)
{
	return htmlspecialchars(utf8_encode($name));
}

function sd_link_path($path)
{
	return SD_WWW_ROOT . $path;
}

function sd_static_path($path)
{
	return '/static/' . $path;
}

function sd_file_exists($path)
{
	return preg_match('{^' . preg_quote(SD_ROOT) . '}', $path)
		&& file_exists($path);
}

function sd_file_is_accessable($path, &$groups = null)
{
	global $file_permissions;

	// If the file is not in the SD_ROOT folder, it is not accessible
	if (substr_compare($path, SD_ROOT, 0, strlen(SD_ROOT)) !== 0)
		return false;

	// For the rest of the calculations, use the path relative to the SD_ROOT.
	$path = substr($path, strlen(SD_ROOT));

	$groups = path_get_access_group($path, $file_permissions);

	switch ($groups[0])
	{
		case 'None':
			return false;

		case 'All':
			return true;

		case 'Member':
			return request_from_cover_gateway() || get_cover_session();

		default:
			return sd_member_in_committees($groups);
	}
}

function sd_member_in_committees($groups)
{
	foreach ($groups as $group)
		if (cover_session_in_committee($group))
			return true;

	return false;
}

function sd_file_is_public($path)
{
	global $file_permissions;

	// If the file is not in the SD_ROOT folder, it is not accessible
	if (substr_compare($path, SD_ROOT, 0, strlen(SD_ROOT)) !== 0)
		return false;

	// For the rest of the calculations, use the path relative to the SD_ROOT.
	$path = substr($path, strlen(SD_ROOT));

	$group = path_get_access_group($path, $file_permissions);

	return $group == 'All';
}

function breadcrumbs($path)
{
	$path = preg_replace('{^' . preg_quote( SD_ROOT . DIRECTORY_SEPARATOR ) . '?}', '', $path);

	$parts = explode(DIRECTORY_SEPARATOR, $path);

	$crumbs = array();

	$sub_path = SD_ROOT;

	$crumbs[] = '<a href="' . sd_link_path('/') . '">Root</a>';

	foreach ($parts as $i => $part)
	{
		$sub_path .= DIRECTORY_SEPARATOR . $part;

		$crumbs[] = sprintf('<a href="%s">%s</a>',
			sd_link_path(urlencode_path($sub_path)), encode_name($part));
	}

	return sprintf('<span class="breadcrumbs">%s</span>', implode(" / ", $crumbs));
}

function array_all($test, array $items)
{
	foreach ($items as $item)
		if (!call_user_func($test, $item))
			return false;

	return true;
}

function is_dot($basename)
{
	return substr($basename, 0, 1) == '.';
}

function is_date($datestring)
{
	return preg_match('/^\d{4}-\d{2}-\d{2}$/', $datestring);
}

function file_permissions_icon($path, &$groups = null)
{
	global $file_permissions;

	// If the file is not in the SD_ROOT folder, it is not accessible
	if (substr_compare($path, SD_ROOT, 0, strlen(SD_ROOT)) !== 0)
		return '<i class="fa fa-fw fa-ban" aria-hidden="true"></i>';

	// For the rest of the calculations, use the path relative to the SD_ROOT.
	$path = substr($path, strlen(SD_ROOT));

	$groups = path_get_access_group($path, $file_permissions);

	switch ($groups[0])
	{
		case 'None':
			return '<i class="fa fa-fw fa-ban" aria-hidden="true"></i>';

		case 'All':
			return '<i class="fa fa-fw fa-globe" aria-hidden="true"></i>';

		case 'Member':
			return '<i class="fa fa-fw fa-user" aria-hidden="true"></i>';

		default:
			return '<i class="fa fa-fw fa-lock" aria-hidden="true"></i>';
	}
}

function display_link($path)
{
	if (strcasecmp(pathinfo($path, PATHINFO_EXTENSION ), 'link') !== 0)
		return false;

	if (($link = @file_get_contents($path)) === false)
		return false;

	$link = trim($link);

	if (filter_var($link, FILTER_VALIDATE_URL) !== false){
		return sprintf('<li class="link" id="%s" tabindex="1"><a href="%s" rel="noopener" target="_blank">%s</a></li>',
				md5($path),
				htmlspecialchars($link),
				encode_name(pathinfo($path, PATHINFO_FILENAME)));
	}

	return false;
}

function dirlisting($directory)
{
	$excluded = ['index.php'];

	$files = scandir($directory);

	// Filter out dot files and files such as index.php
	$files = array_filter($files, function($file) use ($excluded) {
		return !is_dot($file) && !in_array($file, $excluded);
	});

	$is_dirs = array();
	$names = array();

	// Prepare sorting attributes
	foreach ($files as $file)
	{
		$path = $directory . DIRECTORY_SEPARATOR . $file;

		$is_dirs[] = is_dir($path);
		$names[] = basename($file);
	}

	// Sort the files array (directories first, then ordered by filename)
	array_multisort($is_dirs, SORT_DESC, $names, SORT_ASC, $files);

	// If all the files/directories in a folder have a date as name, reverse the order
	// such that the latest set of documents is on top.
	if (array_all('is_date', $names))
		$files = array_reverse($files);

	// will contain the HTML for the list items and subtrees.
	$items = array();

	// Generate html items and recursively subtrees
	foreach ($files as $file)
	{
		$path = $directory . DIRECTORY_SEPARATOR . $file;

		if (is_dir($path))
		{
			$subtree = dirlisting($path);

			// Only show subtree if there is content
			if (strlen($subtree) > 0)
				$items[] = sprintf('<li class="directory" id="%s" tabindex="1"><a href="%s">%s</a>%s</li>',
					md5($path), 
					sd_link_path(urlencode_path($path)), 
					encode_name($file),
					$subtree);
		}
		else if (sd_file_is_accessable($path))
		{
			$link = display_link($path);
			if ($link !== false)
				$items[] = $link;
			else
				$items[] = sprintf('<li class="file" id="%s" tabindex="1" data-preview="%s"><a href="%s" target="_blank">%s</a> <span class="file-size">%s</span> <span class="file-mtime">%s</span><span class="file-permissions">%s</span></li>',
					md5($path),
					sd_link_path(':preview/' . urlencode_path($path)),
					sd_link_path(urlencode_path($path)),
					encode_name($file),
					human_readable_filesize(filesize($path)),
					date('Y-m-d H:i', filemtime($path)),
					file_permissions_icon($path));
		}
	}

	if (count($items) == 0)
		return '';
	else
		return sprintf('<ul>%s</ul>', implode("\n", $items));
}

function main()
{
	if (isset($_GET['path']))
	{
		if ($_GET['path'] == ':search' || $_GET['path'] == '/:search') {
			header('Content-Type: application/json');
			view_search_results($_GET['q']);
		}
		else {

			$path = fsencode_path($_GET['path']);

			if (!sd_file_exists($path))
				view_not_found($path);
			else if (is_dir($path) && sd_file_is_accessable($path, $group))
				view_index($path);
			else if (!sd_file_is_accessable($path, $group))
				view_unauthorized($path, 'This file is only accessible for people of the group ' . implode(',', $group));
			//else if (!empty($_GET['preview']))
			//	view_preview($path);
			else
				view_resource($path);
		}
	}
	else
		view_index(SD_ROOT);
}

function view_not_found($path)
{
	header('Status: 404 Not Found');
	view_layout('File not found', sprintf('File not found: %s', encode_name(basename($path))));
}

function view_unauthorized($path = null, $error = null)
{
	header('Status: 401 Unauthorized');
	$content = '';

	if ($error !== null)
		$content .= sprintf('<div class="alert alert-danger" alert-dismissible" role="alert">'.
			'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
			'<span aria-hidden="true">&times;</span></button>%s</div>',
			htmlspecialchars($error));

	if ($path !== null)
		$content .= sprintf('<p>You need to be logged in onto the Cover website to view <tt>%s</tt></p>', encode_name(basename($path)));

	$content .= '<p><a class="btn btn-primary" href="' . cover_login_url() . '">Login here</a></p>';

	view_layout('Unauthorized', $content);
}

function get_mime_type($file)
{
	if (function_exists("finfo_file"))
	{
		$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
		$mime = finfo_file($finfo, $file);
		finfo_close($finfo);
		return $mime;
	}
	
	else if (function_exists("mime_content_type"))
		return mime_content_type($file);
	
	else if (!stristr(ini_get("disable_functions"), "shell_exec"))
	{
		// http://stackoverflow.com/a/134930/1593459
		$file = escapeshellarg($file);
		$mime = shell_exec('file -bi ' . escapeshellarg($file));
		return $mime;
	}
	
	else
		return null;
}

function view_resource($path)
{
	// header('Cache-control: ' . sd_file_is_public($path) ? 'public' : 'private');

	if ($content_type = get_mime_type($path))
		header('Content-Type: ' . $content_type);
	else {
		$name = pathinfo($path, PATHINFO_FILENAME);
		header('Content-Disposition: attachment; filename="' . $name . '"');
	}

	$fhandle = fopen($path, 'rb');
	fpassthru($fhandle);
	fclose($fhandle);
}

function view_preview($path)
{
	$db = get_db();

	$stmt = $db->prepare("SELECT keywords FROM files WHERE path = :path");
	$stmt->bindParam(':path', $path);
	$stmt->execute();

	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	if (!$result) {
		header('Status: 404 Not Found');
		echo "Preview not available";
	}

	header('Content-Type: text/plain');
	echo $result['keywords'];
}

function view_index($path)
{
	$title = pathinfo($path, PATHINFO_FILENAME);

	$content = breadcrumbs($path);

	$content .= dirlisting($path);

	view_layout($title, $content);
}

function view_search_results($querystring)
{
	$query = parse_search_query($querystring);

	$hits = array();

	$db = get_db();

	$simple_keywords = array();

	foreach ($query as $i => $keyword)
		$simple_keywords[':kw' . $i] = '%' . $keyword . '%';

	// 'Notulen 2014' -> 'path LIKE "%notulen%" AND path LIKE "%2014%"'
	$path_query = implode(' AND ', array_map(function($placeholder) {
		return 'path LIKE ' . $placeholder;
	}, array_keys($simple_keywords)));

	$simple_query = implode(' AND ', array_map(function($placeholder) {
		return 'keywords LIKE ' . $placeholder;
	}, array_keys($simple_keywords)));

	$full_text_query = 'MATCH(keywords) AGAINST (:query IN BOOLEAN MODE)';

	// 'SympoCie terugbetalen' -> '+SympoCie +terugbetalen'
	$full_text_keywords = implode(' ', array_map(function($keyword) {
		return '+"' . $keyword . '"';
	}, $query));

	$stmt = $db->prepare("SELECT uid, path, keywords FROM files WHERE ($path_query) OR ($full_text_query) OR ($simple_query)");

	$stmt->execute(array_merge(
		array(':query' => $full_text_keywords),
		$simple_keywords));

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		if (sd_file_exists($row['path']) && sd_file_is_accessable($row['path']))
			$hits[$row['uid']] = excerpt($row['keywords'], $query);
	}

	header('Content-Type: application/json');
	echo json_encode($hits);
}

function view_layout($title, $content)
{
	header('Content-Type: text/html; charset=UTF-8');
	require 'layout.phtml';
}

main();
